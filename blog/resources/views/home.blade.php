@extends('layout.public')
@section('content')
<div id="home">
    <div class="landing-text">
        <h1>Teknologi Informasi</h1>
    </div>
</div>
<div id="contact_us">
    <div class="container">
        <h1 class="text-center" id="contact">CONTACT US</h1><br>
        <form>
            <div class="form-group">
                <label class="label_form">NAMA</label>
                <input type="text" class="kolom" name="nama">
            </div>
            <div class="form-group">
                <label class="label_form">EMAIL</label>
                <input type="text" class="kolom" name="email">
            </div>
            <div class="form-group">
                <label class="label_form">MESSAGE</label>
                <input type="text" class="kolom" name="message">
            </div>
            <div class="button">
            <button class="btn btn-primary"> Send </button>
            </div>
        </form>
    </div>
</div>
@endsection

      