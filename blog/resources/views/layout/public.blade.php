<html>
    @include('layout.header')
    <body data-spy="scroll" data-target="my_navbar">
                @include('layout.navbar')
                @yield('content')
                @include('layout.footer')
                @include('layout.script')
                @yield('script')
    </body>
</html>
